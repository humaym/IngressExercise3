package Ingress.ms14.hometask3.service;

import Ingress.ms14.hometask3.dto.CarDto;
import Ingress.ms14.hometask3.dto.CreateCarDto;
import Ingress.ms14.hometask3.dto.UpdateCarDto;
import Ingress.ms14.hometask3.model.Car;
import Ingress.ms14.hometask3.repository.CarRepository;
import lombok.Data;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Data
public class CarService {

    private final CarRepository carRepository;
    private final ModelMapper modelMapper;


    public void update(UpdateCarDto dto) {
        Optional<Car> entity = carRepository.findById(dto.getId());
        entity.ifPresent(car1 -> {
            car1.setId(dto.getId());
            car1.setDate(dto.getDate());
//            car1.setColor(dto.getColor());
            car1.setCarEngine(dto.getEngine());
            car1.setModel(dto.getModel());
            car1.setMaker(dto.getMaker());

            carRepository.save(car1);
        });
    }

    public void create(CreateCarDto dto) {
        Car car = modelMapper.map(dto, Car.class);
        carRepository.save(car);
    }

    public CarDto get(Integer id) {
        Car car = carRepository.findById(id).get();
        CarDto carDto = modelMapper.map(car, CarDto.class);
        return carDto;
    }

    public List<CarDto> getAll() {
        List<Car> cars = carRepository.findAll();
        List<CarDto> carDto = cars.stream()
                .map(car -> modelMapper.map(car, CarDto.class))
                .collect(Collectors.toList());

        return carDto;
    }

    public void delete(Integer id) {
        carRepository.deleteById(id);
    }

    public void deleteAll() {
        carRepository.deleteAll();
    }
}
