package Ingress.ms14.hometask3.model;

public final class SingletonClass {

    private static SingletonClass singletonClass;

    public SingletonClass() {
    }

    public static SingletonClass getInstance(){
        if (singletonClass==null){
            synchronized (SingletonClass.class){
                if (singletonClass==null){
                    singletonClass=new SingletonClass();
                }
            }
        }
        return singletonClass;
    }
}
