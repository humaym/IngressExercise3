package Ingress.ms14.hometask3.dto;

import lombok.Data;

import java.time.LocalDate;
import java.time.Year;


@Data
public class CreateCarDto {
    private Integer id;
    private String color;
    private String engine;
    private String model;
    private LocalDate date;
    private String maker;
}
