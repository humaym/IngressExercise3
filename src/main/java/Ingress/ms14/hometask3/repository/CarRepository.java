package Ingress.ms14.hometask3.repository;

import Ingress.ms14.hometask3.model.Car;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CarRepository extends JpaRepository<Car,Integer>
{

}
