package Ingress.ms14.hometask3.controller;

import Ingress.ms14.hometask3.dto.CarDto;
import Ingress.ms14.hometask3.dto.UpdateCarDto;
import Ingress.ms14.hometask3.dto.CreateCarDto;
import Ingress.ms14.hometask3.service.CarService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/car")
public class CarController {

    private final CarService carService;

    public CarController(CarService carService) {
        this.carService = carService;
    }

    @PostMapping
    public void create(@RequestBody CreateCarDto dto) {
        carService.create(dto);
    }

    @PutMapping
    public void update(@RequestBody UpdateCarDto dto) {
        carService.update(dto);
    }

    @GetMapping("/{id}")
    public CarDto get(@PathVariable Integer id) {
        return carService.get(id);
    }

    @GetMapping
    public List<CarDto> getAll(){
       return carService.getAll();
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
        carService.delete(id);
    }

    @DeleteMapping
    public void deleteAll() {
        carService.deleteAll();
    }
}
