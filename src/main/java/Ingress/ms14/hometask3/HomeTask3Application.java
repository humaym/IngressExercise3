package Ingress.ms14.hometask3;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
@RequiredArgsConstructor
@Data
public class HomeTask3Application {


    public static void main(String[] args) {
        SpringApplication.run(HomeTask3Application.class, args);
    }
}

